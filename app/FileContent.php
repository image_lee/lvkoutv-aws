<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileContent extends Model
{
    protected $table = 'com_file_content';
    protected $fillable = [
        'created_by','created_date','file_content_id','file_content_public_yn','file_content_tags',
        'file_description','file_subject','updated_by','updated_date', 'file_total_time'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
}
