<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Randing extends Model
{
    protected $table = 'lk_randing';
    protected $fillable = [
        'email','name','phone1','phone2','phone3','randing_id','updated_date','created_date'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';


}
