<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'lk_video';
    protected $fillable = [
        'video_id','video_id_hash','file_id','subtitle_file_path','subtitle_lang_default','subtitle_lang_json',
        'created_date','updated_date','use_yn','video_bookmark_cnt','video_catogry','video_country','video_description',
        'video_dislike_cnt','video_like_cnt','video_public_yn','video_share_cnt','video_tags','video_thumbnail_src',
        'video_title','video_total_time','video_view_cnt'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
}



