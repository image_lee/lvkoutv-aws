<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'com_file';
    protected $fillable = [
        'file_id','file_seq','file_name','file_path','original_file_name','file_size','file_extension', 'file_type',
        'created_by','created_date','updated_by','updated_date','user_id'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
}


