<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignInAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
            // 'phone' => 'required',confirmed
        ];
    }

    public function messages()
    {
        return [
            'email.required' => '이메일을 입력해 주세요.',
            'email.email'  => '이메일 형식으로 입력해주세요.',
            'password.required' => '비밀번호를 입력해 주세요.', 
        ];
    }
}
