<?php

namespace App\Http\Controllers;

use App\Randing;
use Validator;
use Illuminate\Http\Request;

class RandingController extends Controller
{
    public function index() {
        return view('card');
    }

    public function card(Request $request) {

        $validatedData = $request->validate([
            'email' => 'required|unique:posts',
            'name' => 'required',
            'phone' => 'required'
        ]);

        if($validatedData->fails()) {
            return redirect('card')->withErrors($validatedData)->withInput();
        } else {
            $randing = new Randing;
            $randing->name = $request->name;
            $randing->email = $request->email; 
            $randing->phone1 = $request->phone;

            if($randing->save()) {
                return redirect('card');
            }

        }
    }
}
