<?php

namespace App\Http\Controllers;

use DB, Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\SystemController;

class FeedController extends Controller
{
    protected $path;
    protected $side;

    public function __construct(SystemController $side) {
        $this->side = $side;
    }

    public function index() {
        // $sideNav = $this->side->sideNav();
        return view('index');
    }

    public function trending() {
        $sideNav = $this->side->sideNav();
        return view('feed.trending.video', compact('sideNav'));
    }

    public function live() {
        $sideNav = $this->side->sideNav();
        return view('feed.live', compact('sideNav'));
    }

    public function feed() {
        return view('feed.newsFeed', compact('sideNav'));
    }

    public function category() {
        $sideNav = $this->side->sideNav();

        $brands = DB::table('lk_video')->select('video_id','video_title','video_country', 'video_catogry','video_thumbnail_src', 'video_mov_src')->get();
        return view('feed.biz.category', compact('brands'));
    }

    public function weishang(){
        return view('feed.biz.category');
    }
}

