<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use lluminate\Routing\Router;
// use App\Http\Controllers\Route;

class SystemController extends Controller
{
    public function sideNav() {
        // $side = DB::table('sys_menu')->select('menu_id', 'menu_name_en', 'menu_type_code', 'login_check', 'program_id', 'parent_program_id', 'display_order')
        //                              ->where('menu_type_code', 'SM')->orWhere('menu_type_code', 'ST')->get();

        $side = DB::table('sys_menu as m')->leftJoin('sys_program as p', 'm.program_id', '=', 'p.program_id')
        ->select('m.menu_id','m.menu_name_en','m.menu_type_code','m.display_order','m.program_id','m.parent_program_id','m.use_yn', 'p.program_path')
        ->orderBy('m.display_order', 'asc')->get();
        
        return $side;
        //return response()->json('side', $side);
    }


    public function idCreate(Request $request) {

        return 'id create';
        //if sys_menu - http://www.domain.com/menu/create
        //if video - http://www.domain.com/video/create
        //if product - http://www.domain.com/product/create 
        //if channel - http://www.domain.com/channel/create     
        // $prefix = null;

        // if($request->has('location')) {
            
        //     $location = $request->input('location'); 

        //     if($location == 'menu') {
        //         $prefix = 'MN';
        //         $category_id = $prefix . date("YmdHis");
        //     }

        // } else {}
    }

    public function idStore(Request $request) {}

    public function test(Request $request) { 

        /* $request에 들어올 값  = menu, program, code_group, code_detail, product_id ...*/
        // type = hidden, name=category, value="menu, program, code_group" 등등 

        $category = $request->input('category'); //menu, program, code_group
        $prefix = null;
        $date = date('YmdHis');
        
        if($category == "menu") {
            $prefix = 'MN';
        } 
        else if ($category == "code_group") {
            $prefix = 'CD';
        }  else {
            $prefix = 'XX';
        }
        $category_id = $prefix.$date; //CD000001
        $this->test2($category_id);
    }

    public function test2($category_id) {

        //SEQEUNCE GENERATOR 

    }
}

