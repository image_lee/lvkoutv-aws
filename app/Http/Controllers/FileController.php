<?php

namespace App\Http\Controllers;

use DB, Validator;
use App\File;
use App\FileContent;
use App\Video;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class FileController extends Controller
{
    private $img_ext = ['jpg', 'jpeg', 'png', 'gif'];
    private $vidio_ext = ['mp4', 'mpeg'];

    /*
        1. 파라미터로 타입과 아이디를 받음 
        2. 아이디가 있으면 그 아이디에 해당하는 파일 목록 데이터를 가져옴 
    */
    public function index($type, $id=null) {
        $model = new File();
        if(!is_null($id)) { // 아이디가 null이 아닌 경우 
            return $files;
        } else {
            return 'id is null';
            // $id가 null 인 경우 - 그냥 뿌리는 거고 
        }

        return response()->json($files);
    }

    public function create() {

        $category = DB::table('sys_code_detail')->select('code_group_id', 'code_name', 'display_order')->where('code_group_id', 'VCATEGORY')->orderBy('display_order', 'asc')->get();
        $country = DB::table('sys_code_detail')->select('code_group_id', 'code_name', 'display_order')->where('code_group_id', 'VCOUNTRY')->get();
        return view('admin.video.create', compact('category', 'country'));
   
    }

    public function store(Request $request) {


        $video = new Video;
        $video->video_title = $request->input('title');
        $video->video_id_hash = Hash::make($video->video_title);
        $video->video_description = $request->input('dest');
        // $video->file_total_time = $request->input('total_time');
        $video->video_public_yn =  $request->input('use_yn');
        $video->video_catogry = $request->input('category');
        $video->video_country = $request->input('country');
        // $video->file_id = 'VIDEO'.date("YmdHis");
        $video->file_id = 'VIDEO'.time();


        if($video->save()) {
            return redirect(url("/admin/video/create"));
            // $video->video_mov_src = env('CDN_URL').'video/video_'.$video->video_id.'_mov.mp4';
            // $video->video_thumbnail_src = env('CDN_URL').'video/video_'.$video->video_id.'_img.png';
        } else {
            return false;
        }

        // $path = $file->storeAs( $prefix . '/'.$user_id . '/' . $video->video_catogry, $file_name);

   
        /* S3 store 

        $file = $request->file('file');
        $file_extension = $file->getMimeType();
        $user_id = 1;

        if($file_extension == 'video/mp4') {
            $prefix = 'VIDEO';
        } else {
            $prefix = 'IMG';
        }

        $file_id = $prefix . date("YmdHis");
        $file_seq = 1;
        $file_name = $file->getClientOriginalName();
        $file_path = $file->path();
        $original_file_name = $file->getClientOriginalName();
        $file_size = $file->getClientSize();

        $path = $file->store( $prefix . '/'.$user_id . '/' . $category, 's3');

        if($path) {
            $model = DB::table('com_file')->insert([
                'file_id' => $file_id,
                'file_seq' => $file_seq,
                // 'file_name' => $fileName,
                'file_path' => $file_path,
                'original_file_name' => $original_file_name,
                'file_size' => $file_size,
                'file_type' => $file_extension,
            ]); 

            return redirect('/admin/video/create');
            

        } else {
            return false;
        }

        */
    }
}


