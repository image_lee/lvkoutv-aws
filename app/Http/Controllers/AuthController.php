<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RegisterAuthRequest;
use App\Http\Requests\SignInAuthRequest;
use Illuminate\Support\MessageBag;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller 
{
    /*
    * Create a new AuthController instance.
    * @return voidRequest $request
    */
    public $emailVerifyAfterSignup = true;

    public function signInForm() {
        return view('user.signIn');
    }

    public function signIn(SignInAuthRequest $request) 
    {
        $credentials = $request->only('email', 'password');
        $email = $request->input('email');

        if (! $token = JWTAuth::attempt($credentials)) { //user verity & create token
            return response()->json(['error' => 'invalid_credentials'], 401);
        } else {
            DB::table('com_user')->where('email', $email)->update(['user_status_code' => 'A']);    
            $user_id = DB::table('com_user')->where('email', $email)->value('user_id'); 
            DB::table('com_user_token')->update(['user_id' => $user_id, 'token' => $token]);
            return response()->json(['status' => "success", 'token' => $token], 200);
        }
    }

    /*
        @param $request (array)
    */
    public function signUp(RegisterAuthRequest $request) 
    {
       
        // $validated = $request->validated();

        $user = new User();
        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->password = Hash::make($request->input('password'));

        $token = JWTAuth::fromUser($user);

        if($user->save()) {
            return response()->json(['status' => 'success', 'msg' => 'User successfully sign up', 'user' => $user, 'token' => $token], 200);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'User sign up error'], 500);
        }

        // return back()->with('success', 'User created successfully.');
    }

    //@return 인증 된 사용자
    public function getAuthUserData(Request $request) 
    {
        $this->validate($request, ['token' => "required"]);
        $user = JWTAuth::authenticate($request->token);

        if(! $user) {
            return response()->json(['user' => $user]);
        } 
    }

    public function logout(Request $request) 
    {
        //$token = $request->header('Authorization'); //get jwt token from request header 

        try {
            JWTAuth::invalidate($token);
            return response()->json([
                'status' => 'success', 
                'message'=> "User successfully logged out."
            ]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                'status' => 'error', 
                'message' => 'Failed to logout, please try again.'
            ], 500);
        }
    }
     
    public function refresh() {} // 토큰 재발행 
    public function verifyUser() {} //


    public function findEmail() {
        return view('user.password.findEmail');
    }

    /*
    * 계정을 찾으면 이메일보내기 / 아니면 원래 페이지로 back()->withInput() 
    * @param email 
    */
    public function resetSentEmail(Request $request) {
        $validator = $request->validate([
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            //return redirect('post/create')->withErrors($validator)->withInput();
        }

        if($request->has('email') &&  $request->filled('email')) {
            $email = $request->input('email');
            $emailAccount = DB::table('com_user')->where('email', $email)->first();
            if(count($emailAccount) > 0) {
                // 이 이메일 계정 $emailAccount으로 메일을 보냄 
            } else {
                return back(); // 없는 계정입니다 문구 
            }
        } else {
            return back()->with();
        }
    }

    /*
    * 비밀번호 재설정 
    * @param email 
    */
    public function resetPassword(Request $request) {
        if($request->filled('password') && $request->filled('passwordConfirm') ) {
            $pwd = $request->input('password'); 
            $pwdConfirm = $request->input('passwordConfirm');

            if($pwd == $pwdConfirm) {
                return response()->json('same password');
            }
        } else {
            
            // return response()->json(false);
        }
        // $user = DB::table('com.user')('email', $email)->first();
        // if(!$user) {
        //     $error_message = "Your email address was not found. Please enter your email again";
        //     return response()->json(['status' => 'success', 'msg' => $error_message, 'data' => $user], 200);
        // } else {
        //     // $user['id']
        // }
    }    
}

