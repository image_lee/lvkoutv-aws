<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MypageController extends Controller
{
    protected $user; 

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index() {
        return $this->user;
    }

}
