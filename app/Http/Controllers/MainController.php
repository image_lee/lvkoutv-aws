<?php

namespace App\Http\Controllers;

use DB, Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\SystemController;

class MainController extends Controller
{

    public function upload() {
        return view('upload');
    }

    public function video($id) {
        $video = DB::table('lk_video')->where('video_id', $id)->first();
        return view('detail', compact('video')); 
    }

    public function network() {
        // $sideNav = $this->side->sideNav();
        return view('beautyNetworks');
    }

    public function jobs() {
        // $sideNav = $this->side->sideNav();
        return view('jobs');
    }

    public function news() {
        // $sideNav = $this->side->sideNav();
        return view('news');
    }

}

