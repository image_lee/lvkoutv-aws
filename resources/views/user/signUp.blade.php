@extends('layouts.auth')
@section('title', 'Welcome')
@section('authContent')

<div class="login-wrapper">
    <div class="lk-bg-screen-top"></div>
    <div class="container" style="padding: 100px;">
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="logo-img">
                    <a href="{{ url('/')}}"><img id="logo-img" width="150" height="40" src="/img/header/lvkoutv_log_white.png" alt="LvkouTV Logo"></a>
                </div>
                <!-- @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif -->
                <div class="card">
                    <div class="card-left">
                        <form class="lk-login-form" method="POST" action="{{ url('api/user/signUp') }}">
                            <div class="lk-login-header"><h2>Sign Up</h2></div>
                            @csrf
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div>
                                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email">
                                    @if (count($errors) > 0)
                                        @foreach($errors->get('email') as $message)
                                        <div class="invalid-masseage">{{ $message }}</div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Name">
                                    @if (count($errors) > 0)
                                        @foreach($errors->get('name') as $message)
                                        <div class="invalid-masseage">{{ $message }}</div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div>
                                    <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" placeholder="Password">
                                    @if (count($errors) > 0)
                                        @foreach($errors->get('password') as $message)
                                        <div class="invalid-masseage">{{ $message }}</div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="policy">
                                회원가입시 이용약관 및 쿠키사용, 개인정보처리방침에 동의하게 됩니다.
                            </div>

                            <div class="lk-login-submit-btn">
                                <button type="submit" class="btn btn-secondary btn-lg btn-block">SIGNUP</button>
                            </div>
                        </form>
                    </div>
                </div><!-- card -->
            </div><!-- row -->
        </div>
    </div>
<div>
@endsection        

