@extends('layouts.auth')
@section('title', 'Welcome')
@section('authContent')

<div class="login-wrapper">
    <div class="lk-bg-screen-top"></div>
    <div class="container" style="padding: 100px;">
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="logo-img">
                    <a href="{{ url('/')}}"><img id="logo-img" width="150" height="40" src="/img/header/lvkoutv_log_white.png" alt="LvkouTV Logo"></a>
                </div>
                <div class="card">
                    <div class="card-left">
                        <form class="lk-login-form" method="POST" action="{{ url('user/login') }}">
                            <div class="lk-login-header"><h2>Login</h2></div>
                            @csrf
                            <div class="form-group">
                                <div>
                                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email">
                                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <input type="password" class="form-control" id="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="form-group form-check">
                                    <div class="remember-me">
                                        <input type="checkbox" class="form-check-input" id="remember">
                                        <label class="form-check-label" for="exampleCheck1">Remember me</label>
                                    </div>
                                </div>
                                <div class="forget">
                                    <a href="{{ url('/password/find') }}">Forgot Password?</a>    
                                </div>
                            </div>
                            <div class="lk-login-submit-btn">
                                <button type="submit" class="btn btn-secondary btn-lg btn-block">LOGIN</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-right">
                        <div class="lk-singup">
                            <p class="no-account">No Account :</p>
                            <a href="{{ url('user/signUp') }}">
                                <h5 class="join">Join Us!</h5>
                            </a>
                        </div>
                        <div class="social-links">
                            <p class="login-easily">Login easily :</p>
                            <ul class="social-links-list">
                                <li class="social-links-item gcolor">
                                    <a><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li class="social-links-item fcolor">
                                    <a><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="social-links-item wcolor">
                                    <a><i class="fa fa-wechat"></i></a>
                                </li>
                            <ul>  
                            <ul class="social-links-list">
                                <li class="social-links-item icolor">
                                    <a><i class="fa fa-instagram"></i></a>
                                </li>
                                <li class="social-links-item tcolor">
                                    <a><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="social-links-item weicolor">
                                    <a><i class="fa fa-weibo"></i></a>
                                </li>
                            <ul>    
                            <!-- <div class="row">
                                <div class="col">
                                    <a><i class="fa fa-facebook"></i></a>
                                </div>
                                <div class="col"></div>
                                <div class="col"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>
@endsection        

