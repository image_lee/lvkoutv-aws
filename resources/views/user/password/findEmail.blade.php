@extends('layouts.auth')
@section('title', 'Welcome')
@section('authContent')
<div class="password-wrapper">
    <div class="lk-bg-screen-top"></div>
    <div class="container" style="padding: 100px;">
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="logo-img">
                    <a href="{{ url('/')}}"><img id="logo-img" width="150" height="40" src="/img/header/lvkoutv_log_white.png" alt="LvkouTV Logo"></a>
                </div>
                <div class="card">
                    <div>
                        <form class="lk-password-form" method="POST" action="{{ url('password/resetSentEmail') }}">
                            <div class="lk-password-header">
                                <h2>Find your Email account</h2>
                                <p>Enter your email</p>
                            </div>
                            @csrf
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                            </div>
                            <div class="lk-password-submit-btn">
                                <button type="submit" class="btn btn-secondary btn-lg btn-block">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>
@endsection        


