@extends('layouts.auth')
@section('title', 'Welcome')
@section('authContent')
<div class="password-wrapper">
    <div class="lk-bg-screen-top"></div>
    <div class="container" style="padding: 100px;">
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="logo-img">
                    <a href="{{ url('/')}}"><img id="logo-img" width="150" height="40" src="/img/header/lvkoutv_log_white.png" alt="LvkouTV Logo"></a>
                </div>
                <div class="card">
                    <div class="lk-password-success">
                        <h3>Congratulations! </br>
                        You’ve successfully changed your password. </h3>
                        <a href="{{ url('/') }}"><p>Continue to Lvkoutv</p></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>
@endsection