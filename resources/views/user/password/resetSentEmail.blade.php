@extends('layouts.auth')
@section('title', 'Welcome')
@section('authContent')
<div class="password-wrapper">
    <div class="lk-bg-screen-top"></div>
    <div class="container" style="padding: 100px;">
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="logo-img">
                    <a href="{{ url('/')}}"><img id="logo-img" width="150" height="40" src="/img/header/lvkoutv_log_white.png" alt="LvkouTV Logo"></a>
                </div>
                <div class="card">
                    <div class="lk-password-sent-email">
                        <div class="lk-password-sent-email-header">
                            <h2>Please check your email and click the link</h2>
                            <p>We just email you a link</p>
                        </div>
                        <div class="lk-password-submit">
                            <button type="button" class="btn btn-secondary btn-lg btn-block">Resend</button>
                            <!-- <button type="submit" class="btn btn-secondary btn-lg btn-block">resend</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>
@endsection