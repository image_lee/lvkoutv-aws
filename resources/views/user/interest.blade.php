@extends('layouts.auth')
@section('title', 'Welcome')
@section('authContent')
<div class="interest-wrapper">
    <div class="lk-bg-screen-top"></div>
    <div class="container" style="padding: 100px;">
        <div class="row justify-content-center">
            <div class="col-auto">
                <div class="logo-img">
                    <a href="{{ url('/')}}"><img id="logo-img" width="150" height="40" src="/img/header/lvkoutv_log_white.png" alt="LvkouTV Logo"></a>
                </div>
                <div class="card">
                    <div>
                        <form class="lk-interest-form" method="POST" action="{{ url('password/resetSentEmail') }}">
                            @csrf
                            <div class="lk-interest-header">
                                <h5>Register Successfully !</h5>
                                <p>Choose your interest tags</p>
                            </div>
                                <div class="beauty-btn-section">
                                    <div class="btn-row d-flex flex-row justify-content-around">
                                        <button class="btn btn-beauty">Beauty</button>
                                        <button class="btn btn-beauty">KOL</button>
                                        <button class="btn btn-beauty">wanghong</button>
                                        <button class="btn btn-beauty">Distributor</button>
                                    </div>
                                    <div class="btn-row d-flex flex-row justify-content-around">
                                        <button class="btn btn-beauty">OEM/ODM</button>
                                        <button class="btn btn-beauty">Seller</button>
                                        <button class="btn btn-beauty">Seller</button>
                                        <button class="btn btn-beauty">Seller</button>
                                    </div>
                                </div>
                            <div class="lk-interest-submit-btn d-flex justify-content-around">
                                <button type="submit" class="btn btn-secondary btn-lg btn-block">SKIP</button>
                                <button type="button" class="btn btn-secondary btn-lg btn-block">SAVE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>
@endsection