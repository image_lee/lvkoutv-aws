@extends('layouts.lvkoutv')
@yield('title', 'Welcome to 吕口TV')
@section('content')
<div class="row lk-row">
<div class="float-left">
        <div class="profile-section lk-card profile">
            <div class="profile-img">
                <img src="/img/people/12.jpg" alt="">
            </div>
            <div class="profile__info">
                <span class="label label-warning">Company</span>
                <span class="label label-success">Premium Member</span>

                <div class="profile__review">
                    <span>Jermaine S. Wilson / Distributor</span>
                </div>
                <ul class="rmd-contact-list">
                    <li><i class="zmdi zmdi-face"></i>Followers: 124</li>
                    <li><i class="zmdi zmdi-phone"></i>308-360-8938</li>
                    <li><i class="zmdi zmdi-email"></i>malinda@inbound.plus</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="float-left">
        <div class="beauty-btn-section">
            <div class="btn-row d-flex flex-row">
                    <button class="btn btn-beauty">Seller <span class="btn-count">(2/100)</span></button>
                    <button class="btn btn-beauty">Buyer <span class="btn-count">(2/100)</span></button>
                    <button class="btn btn-beauty">ODM / OEM <span class="btn-count">(2/100)</span></button>
                    <button class="btn btn-beauty">MRT / PR <span class="btn-count">(2/100)</span></button>
            </div>
            <div class="btn-row d-flex flex-row">
                <button class="btn btn-beauty">KOL / Influencer <span class="btn-count">(2/100)</span></button>
                <button class="btn btn-beauty">Logistics <span class="btn-count">(2/100)</span></button>
                <button class="btn btn-beauty">Others <span class="btn-count">(2/100)</span></button>
                <!-- <button class="btn btn-beauty">Seller</button> -->
            </div>
        </div>   
    </div>
    <div class="lk-row">
        <div class="jobs-section">
            <div class=""><h5>Recommand</h5></div>
            <div class="">
                <ul class="job-card-list d-flex">
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/1.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/16.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/5.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/7.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <ul class="job-card-list d-flex">
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/2.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/4.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/6.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/9.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <ul class="job-card-list d-flex">
                    <li class="job-card-item job-card">
                        <div class="job-card-logo"><img width="72" height="72" src="/img/logo/10.jpg" alt="" /></div>
                        <div class="job-card-detail">
                            <h5>Software Engineer</h5>
                            <p>LvkouTV Corporation, Shanghai, CN</p>
                        </div>
                        <div class="job-card-footer">
                            <p>1 week ago</p>
                        </div>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/1.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/1.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                    <li class="job-card-item job-card">
                        <a>
                            <div class="job-card-logo"><img width="72" height="72" src="/img/logo/1.jpg" alt="" /></div>
                            <div class="job-card-detail">
                                <h5>Software Engineer</h5>
                                <p>LvkouTV Corporation, Shanghai, CN</p>
                            </div>
                            <div class="job-card-footer">
                                <p>1 week ago</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>  
@endsection