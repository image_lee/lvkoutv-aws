@extends('layouts.lvkoutv')
@section('content')
<div class="index-detail-section lk-row">
    <div class="player-section">
        <video controls controlsList="nodownload" preload="auto" width="100%" id="video" data-setup="{}" crossorigin="anomymous">
            <source src="{{ $video->video_mov_src }}" type="video/mp4"/>
        </video>
        <div class="player-body">
            <h3>Alkemilla - Eco COSMETIC</h3>
            <small>362,488 views | Published on 6 Jun 2018</small>
            <ul class="player-icon-list circle list-unstyled">
                <li class="icon-item view"><a href="javascript:void(0)" class="btn btn-sm btn-default-transparent btn-animated"><i class="zmdi zmdi-play"></i></a></li>
                <li class="icon-item like"><a href="javascript:void(0)" class="btn btn-sm btn-default-transparent btn-animated"><i class="zmdi zmdi-favorite-outline"></i></a></li>
                <li class="icon-item watch"><a href="javascript:void(0)" class="btn btn-sm btn-default-transparent btn-animated"><i class="zmdi zmdi-time"></i></a></li>
                <li class="icon-item share"><a href="javascript:void(0)" class="btn btn-sm btn-default-transparent btn-animated"><i class="zmdi zmdi-share"></i></a></li>
            </ul>
            <div class="detail-channel">
                <a href="javascript:void(0)">
                    <img src="/img/people/4.jpg"/>                                    
                </a>
                <div class="detail-channel-info">
                    <h3>Alkemilla Eco Bio Cosmetic</h3>
                    <small>I am the technical director of my company which is called Aroma 我是我们Aroma公司的技术总监
                    </small>
                    <div class="follow-btn">
                        <button class="follow-btn">
                            <img src="/img/icon/follow_sm_default.png"/>
                            Follow
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="next-play-section">
        <div class="n-title"><h3>Next Video</h3></div>
        <div class="n-playlist">
            <div class="n-playlist-item">
                <a href="#" class="media">
                    <div class="float-left">
                        <img src="/video/main/sample/2017.11. Cosmoprof Hongkong2.jpeg" class="playlist-img" alt="">                                        
                    </div>
                    <div class="playlist-body">
                        <h3>Alkemilla Eco COSMETIC</h3>
                        <small>Cosmax</small>
                    </div>
                </a>
            </div>
            <div class="n-playlist-item">
                <a href="" class="media">
                    <div class="float-left">
                        <img src="/video/main/sample/2017.11. Cosmoprof Hongkong1.jpeg" class="playlist-img" alt="">                                        
                    </div>
                    <div class="playlist-body">
                        <h3>2017.11. Cosmoprof Hongkong</h3>
                        <small>Hongkong</small>
                    </div>
                </a>
            </div>
            <div class="n-playlist-item">
                <a href="" class="media">
                    <div class="float-left">
                        <img src="/video/main/sample/2017.11. Cosmoprof Hongkong3.jpeg" class="playlist-img" alt="">                                        
                    </div>
                    <div class="playlist-body">
                        <h3>2017.11. Cosmoprof Hongkong</h3>
                        <small>Hongkong</small>
                    </div>
                </a>
            </div>
            <div class="n-playlist-item">
                <a href="" class="media">
                    <div class="float-left">
                        <img src="/video/main/sample/2017.11. Cosmoprof Hongkong4.jpeg" class="playlist-img" alt="">                                        
                    </div>
                    <div class="playlist-body">
                        <h3>2017.11. Cosmoprof Hongkong</h3>
                        <small>Hongkong</small>
                    </div>
                </a>
            </div>
            <div class="n-playlist-item">
                <a href="" class="media">
                    <div class="float-left">
                        <img src="/video/main/sample/2017.11. Cosmoprof Hongkong5.jpeg" class="playlist-img" alt="">                                        
                    </div>
                    <div class="playlist-body">
                        <h3>2017.11. Cosmoprof Hongkong</h3>
                        <small>Hongkong</small>
                    </div>
                </a>
            </div>
            <div class="n-playlist-item">
                <a href="" class="media">
                    <div class="float-left">
                        <img src="/video/main/sample/BMLcosmetics.jpeg" class="playlist-img" alt="">                                        
                    </div>
                    <div class="playlist-body">
                        <h3>BMLcosmetics</h3>
                        <small>Cosmax</small>
                    </div>
                </a>
            </div>
            <div class="n-playlist-item">
                <a href="" class="media">
                    <div class="float-left">
                        <img src="/video/main/sample/Beauty Insider.jpeg" class="playlist-img" alt="">                                        
                    </div>
                    <div class="playlist-body">
                        <h3>Beauty Insider</h3>
                        <small>Cosmax</small>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="comment-section" style="margin-top:30px">
        <div class="comment-header">
            <h3>Comments 1.2K</h3>
        </div>
        <div clas="comment-list">
            <div class="comment-list-item media">
                <a href="javascript:void(0)">
                    <img src="/img/people/6.jpg"/>
                    <div class="comment-up"><span>+3</span></div>
                </a>
                <div class="comment-body comment-text">
                    <strong>Melsa Taylor</strong>
                    <small>Monday 19th July, 2016 at 7:00 pm</small>
                    <p>I was just thinking about trying the Benefit Porefessional primer 
                        but the price made me reconsider for the time being, so thank you sooooo 
                        much for showing a more affordable alternative. I really love seeing your East vs. West videos.
                    </p>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection