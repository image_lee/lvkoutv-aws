@extends('layouts.lvkoutv')
@section('content')

  <script type="text/javascript">
    $(document).ready(function(){
      $('.ads-main-item').slick({
        autoplay: true, //자동슬라이드
        autoplaySpeed: 1200,
        // slidesToShow: 1, //큰이미지 몇개 보여줄것인지
        // slidesToScroll: 1,
        arrows: false,
        infinite: true,
        variableWidth: true,
        adaptiveHeight: true,
        // dots: true,
        // fade: false,
      });
    });
  </script>

<div class="row ads-section lk-row" style="flex-wrap: nowrap;">
    <div class="ads-main-item">
        <a href="javascript:void(0)">
            <div class="ads-main">
                <img class="ads-img" src="/sample/img/lotus0.png" alt="ads image">
            </div>
            <!-- <div class="ads-main-body">
                <h3 id="ads-title">Sampar Pure perfection kit</h3>
                <small id="ads-channl">THE LOTUS</small>
            </div> -->
        </a>
        <a href="javascript:void(0)">
            <div class="ads-main">
                <img class="ads-img" src="/video/main/sample/video_1_img.png" alt="ads image">
            </div>
        </a>
        <a href="javascript:void(0)">
            <div class="ads-main">
                <img class="ads-img" src="/video/main/sample/banner2.jpeg" alt="ads image">
            </div>
        </a>
        <a href="javascript:void(0)">
            <div class="ads-main">
                <img class="ads-img" src="/video/main/sample/banner3.jpeg" alt="ads image">
            </div>
        </a>
    </div>
    <div class="ads-channel-list"> 
        <div class="ads-channel-item ads-chsize" style="margin-right: 20px;">
            <img src="/img/people/12.jpg" alt="">
            <div class="channel-info">
                <h4>Jermaine S. Wilson</h4>
                <small>Distributor</small>
            </div>
        </div>
        <div class="ads-channel-item ads-chsize">
            <img src="/img/people/7.jpg" alt="">
            <div class="channel-info">
                <h4>Jermaine S. Wilson</h4>
                <small>Chief Executive Officer</small>
            </div>
        </div>
    </div>
        <!-- <div class="row">
            <div class="ads-channel-list">
                <div class="ads-channel-item ads-chsize">
                    <img width="100" height="100" src="./img/sample/brand5.jpg" class="channel-img" alt="">
                </div>
            </div>
            <div class="ads-channel-list">
                <div class="ads-channel-item ads-chsize">
                    <img width="100" height="100" src="./img/sample/brand5.jpg" class="channel-img" alt="">
                </div>
            </div>
        </div> -->
</div>

<!-- <div class="recent-section lk-row">
    <div class="section-title"><h2>Recent</h2></div>
    <div class="recent-main">
        <div class="recent-grid-item">
            <div class="recent-grid-main overlay-container">
                <img src="./img/sample/s6.jpg">
                <a href="#" class="overlay-link"></a>
            </div>
            <div class="overlay-bottom recent-grid-body">
                <h3>Alkemilla - Eco COSMETIC</h3>
                <small>Alkemilla Eco Bio Cosmetic</small>
                <i class="zmdi zmdi-favorite-outline"></i>
            </div>
        </div>
    </div>
    <div class="recent-sub">
        <div class="recent-sub-list sub-top">
            <div class="recent-sub-item">
                <div class="overlay-container">
                        <img src="./img/sample/s7.jpg">
                        <a href="#" class="overlay-link"></a>
                </div>
                <div class="overlay-bottom">
                    <h3>Alkemilla - Eco COSMETIC</h3>
                    <small>Alkemilla Eco Bio Cosmetic</small>
                </div>
            </div>
            <div class="recent-sub-item">
                <a href="#" class="overlay-container">
                    <div class="recent-sub-main">
                        <img src="./img/sample/s7.jpg">
                    </div>
                    <div class="overlay-bottom">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
            <div class="recent-sub-item">
                <a href="#" class="overlay-container">
                    <div class="recent-sub-main">
                        <img src="./img/sample/s7.jpg">
                    </div>
                    <div class="overlay-bottom">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="recent-sub-list sub-top">
            <div class="recent-sub-item">
                <div class="overlay-container">
                        <img src="./img/sample/s7.jpg">
                        <a href="#" class="overlay-link"></a>
                </div>
                <div class="overlay-bottom">
                    <h3>Alkemilla - Eco COSMETIC</h3>
                    <small>Alkemilla Eco Bio Cosmetic</small>
                </div>
            </div>
            <div class="recent-sub-item">
                <a href="#" class="overlay-container">
                    <div class="recent-sub-main">
                        <img src="./img/sample/s7.jpg">
                    </div>
                    <div class="overlay-bottom">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
            <div class="recent-sub-item">
                <a href="#" class="overlay-container">
                    <div class="recent-sub-main">
                        <img src="./img/sample/s7.jpg">
                    </div>
                    <div class="overlay-bottom">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div> -->

<div class="video-section lk-row">
    <div class="section-title"><h2>Recommended videos</h2></div>
    <div class="video-grid-list">
        <div class="listings-grid-item listings-bg-color">
            <a href="{{ url('video/detail/13') }}">
                <div class="listings-grid-main">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_12 TILLEY_EN_호주.png">
                    <span class="time">8:00</span>
                </div>
                <div class="listings-grid-body">
                    <h3>Tilley_2018. May</h3>
                    <small>Australia</small>
                </div>
            </a>
        </div>
        <div class="listings-grid-item listings-bg-color">
            <a href="{{ url('video/detail/12') }}">
                <div class="listings-grid-main">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_26 JBP_EN_일본.png">
                    <span class="time">3:00</span>
                </div>
                <div class="listings-grid-body">
                    <h3>JBP_2018. May</h3>
                    <small>Japan</small>
                </div>
            </a>
        </div>
        <div class="listings-grid-item listings-bg-color">
            <a href="{{ url('video/detail/11') }}">
                <div class="listings-grid-main">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_19 Bxpression_EN_미국.png">
                    <span class="time">4:00</span>
                </div>
                <div class="listings-grid-body">
                    <h3>Bxpression_2018. March</h3>
                    <small>USA</small>
                </div>
            </a>
        </div>
        <div class="listings-grid-item listings-bg-color">
            <a href="{{ url('video/detail/10') }}">
                <div class="listings-grid-main">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_40 S.Ishira_카타르.png">
                    <span class="time">1:30</span>
                </div>
                <div class="listings-grid-body">
                    <h3>S.Ishira_2018. May</h3>
                    <small>Qatar</small>
                </div>
            </a>
        </div>
        <div class="listings-grid-item listings-bg-color">
            <a href="{{ url('video/detail/9') }}">
                <div class="listings-grid-main">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_41 GIGI_이스라엘.png">
                    <span class="time">2:00</span>
                </div>
                <div class="listings-grid-body">
                    <h3>Gigi_2018. March</h3>
                    <small>Israel</small>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="video-section lk-row">
        <div class="section-title"><h2>Trending</h2></div>
        <div class="video-grid-list">
            <div class="listings-grid-item">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/video/main/marketing/A good Marketing agency in China (Shanghai).png">
                        <span class="time">1:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>A good Marketing agency in China (Shanghai).png</h3>
                        <small>shanghai</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/video/main/marketing/China Marketing  Digital Ad Agency China Beijing Shenzhen Shanghai.png">
                        <span class="time">3:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>China Marketing Digital Ad Agency China Beijing Shenzhen Shanghai</h3>
                        <small>shanghai</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/video/main/marketing/Marketing and Distribution Trends in Beauty and Personal Care in China.png">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Marketing and Distribution Trends in Beauty and Personal Care in China</h3>
                        <small>shanghai</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/video/main/marketing/Skincare Brands' China Influencer Marketing Performance   Episode 8.png">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Skincare Brands' China Influencer Marketing Performance Episode 8</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_26 JBP_EN_일본.png">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
        </div>
</div>

@endsection