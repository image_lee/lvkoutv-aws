@extends('layouts.lvkoutv')
@yield('title', 'Welcome to 吕口TV')
@section('content')
<div class="channel-biz-section">
    <div class="channel-profile-section">
        <div style="background: url(./img/channel/ch_bg1.png)" class="ch-profile-bg-img ch-profile-bg-img-loading"></div>
        <div class="row ch-social-link">
            <div class="col ch-social-link">
                <img src="./img/channel/social1.png"/>
                <img src="./img/channel/social2.png"/>
                <img src="./img/channel/social3.png"/>
                <div class="w-100"></div>
                <img src="./img/channel/social4.png"/>
                <img src="./img/channel/social5.png"/>
                <img src="./img/channel/social6.png"/>
            </div>
        </div>
        <div class="ch-profile-container">
            <div class="ch-profile-container profile">
                <img src="./img/blank_ch.png"/>
                <div class="ch-profile-info text-center">
                    <h4>Kay C. Lowery</h4>
                </div>
            </div>
            <button class="btn btn-subscribe">Follow
                <i class="zmdi zmdi-notifications"></i>
            </button>
        </div> 
        <div class="lk-ch-card">
            <div class="tab-nav tab-nav--justified profile-tab">
                <div class="tab-nav__inner">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li class="active"><a href="./channel-biz-brand.html">Brands</a></li>
                        <li><a href="./channel-biz-video.html">Videos</a></li>
                        <li><a href="./channel-biz-jobs.html">Jobs</a></li>
                        <li><a href="./channel-biz-contactus.html">Contact us</a></li>
                        <li><a href="./channel-biz-company-life.html">Company Life</a></li>
                    </ul>
                </div>
            </div>
            <div class="ch-home-tab">
                <div class="ch-page-title"><h3>Main Product</h3></div>
                <div class="separator-2"></div> 
                <div class="">
                    <div class="float-left">
                        <img src="./img/video/sample4.png" width="400" height="250"/>
                    </div>
                    <div class="ch-home-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                            laboris nisi ut aliquip ex. Suspendisse aliquet imperdiet commodo. Aenean vel lacinia elit.
                                Class aptent taciti sociosqu ad litora torquent per.
                        </p>
                        <ul class="ch-home-list circle text-center">
                            <li class="ch-home-item"><a><img src="./img/blank_ch.png"/></a></li>
                            <li class="ch-home-item"><a><img src="./img/blank_ch.png"/></a></li>
                            <li class="ch-home-item"><a><img src="./img/blank_ch.png"/></a></li>
                            <li class="ch-home-item"><a><img src="./img/blank_ch.png"/></a></li>
                        </ul>
                        <div class="text-center">
                            <button class="btn btn-ch-home">Contact Us</button>
                        </div>
                    </div>
                </div>
            </div>

            

        </div>

    </div>
</div>
@endsection