@extends('layouts.lvkoutv')
@yield('title', 'Welcome to 吕口TV')
@section('content')
<div class="channel-biz-section">
    <div class="channel-profile-section">
        <div style="background: url(./img/channel/ch_bg1.png)" class="ch-profile-bg-img ch-profile-bg-img-loading"></div>
        <div class="row ch-social-link">
            <div class="col ch-social-link">
                <img src="./img/channel/social1.png"/>
                <img src="./img/channel/social2.png"/>
                <img src="./img/channel/social3.png"/>
                <div class="w-100"></div>
                <img src="./img/channel/social4.png"/>
                <img src="./img/channel/social5.png"/>
                <img src="./img/channel/social6.png"/>
            </div>
        </div>
        <div class="ch-profile-container">
            <div class="ch-profile-container profile">
                <img src="./img/blank_ch.png"/>
                <div class="ch-profile-info text-center">
                    <h4>Kay C. Lowery</h4>
                </div>
            </div>
            <button class="btn btn-subscribe">Follow
                <i class="zmdi zmdi-notifications"></i>
            </button>
        </div> 
        <div class="lk-ch-card">
            <div class="tab-nav tab-nav--justified profile-tab">
                <div class="tab-nav__inner">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="./channel-biz-brand.html">Brands</a></li>
                        <li class="active"><a href="./channel-biz-video.html">Videos</a></li>
                        <li><a href="./channel-biz-jobs.html">Jobs</a></li>
                        <li><a href="./channel-biz-contactus.html">Contact us</a></li>
                        <li><a href="./channel-biz-company-life.html">Company Life</a></li>
                    </ul>
                </div>
            </div>
            <div class="ch-home-tab">
                <div class="">
                    <div class="ch-brand-section">
                        <div class="section-title"><h2>Recent</h2></div>
                        <div class="video-grid-list">
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="ch-brand-section">
                        <div class="section-title"><h2>Popular</h2></div>
                        <div class="video-grid-list">
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="ch-brand-section">
                        <div class="section-title"><h2>Upload</h2></div>
                        <div class="video-grid-list">
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                            <div class="listings-grid-item">
                                <a href="#">
                                    <div class="listings-grid-main">
                                        <img src="./img/sample/s7.jpg">
                                        <span class="time">8:00</span>
                                    </div>
                                    <div class="listings-grid-body">
                                        <h3>Alkemilla - Eco COSMETIC</h3>
                                        <small>Alkemilla Eco Bio Cosmetic</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

        </div>

    </div>
</div>
@endsection