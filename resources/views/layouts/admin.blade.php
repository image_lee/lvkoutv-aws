<!DOCTYPE html>
<html lang="utf-8">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/sb-admin-2.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="/css/material-design-iconic-font.min.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="/js/jquery-3.3.1.min.js"></script>
    <!-- <link href="/dist/css/jquery-ui.css" rel="stylesheet">
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery/jquery-ui.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="/vendor/metisMenu/metisMenu.min.js"></script>


    <!-- DataTables JavaScript -->
    <script src="/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="/js/sb-admin-2.js"></script>

    <!-- toastr -->
    <!-- <link href="/dist/css/toastr.min.css" rel="stylesheet"/>
    <script src="/dist/js/toastr.min.js"></script> -->

    <script>

    function fileFormSubmit() {


    }
    
    $(document).ready(function() {
        var table;      
        table = $('#dataTables').DataTable({
            //  "order": [ [0,'desc'] ],
            responsive: true
        });
        
        
          
        // $("#dataTables").on("mousedown", "td .zmdi.zmdi-close", function(e) {
        //     table.row($(this).closest("tr")).remove().draw();
        // })

        $("#dataTables").on("mousedown.edit", "td .zmdi.zmdi-edit", function(e) {
            table.row(this).edit();
            // $(this).removeClass().addClass("zmdi zmdi-check");
            // var $row = $(this).closest("tr").off("mousedown");
            // var $tds = $row.find("td").not(':last');

            // $.each($tds, function(i, el) {
            //     var txt = $(this).text();
            //     $(this).html("").append("<input type='text' value=\""+txt+"\">");
            // });

            // $("#dataTables").on('mousedown', "input", function(e) {
            //     e.stopPropagation();
            // });

            // console.log($tds);
        })

    });
    </script>
</head>

<body>
 @if(session()->has('success'))
   <script>
    toastr.success("{{session('success')}}");
   </script>
 @endif

  @if(session()->has('error'))
   <script>
    toastr.warning("{{session('error')}}");
   </script>
 @endif

@yield('admin_contents')
</body>
</html>
