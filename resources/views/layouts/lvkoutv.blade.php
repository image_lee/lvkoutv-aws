<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>吕口TV @yield('title')</title>
    <link rel="icon" type="image/png" href="icon/favicon.png">
    <!-- Bootstrap v4.1.1 -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-grid.min.css" rel="stylesheet">
    <!-- CSS -->
    <link href="/css/index.css" rel="stylesheet">
    <link href="/css/roost.css" rel="stylesheet">

    <!-- Dropzone CSS 5.4.0 -->
    <link href="/css/dropzone.css" rel="stylesheet">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css" rel="stylesheet"> -->
    <!-- Font CSS -->
    <link href="/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/css/material-design-iconic-font.min.css" rel="stylesheet">
    <!-- Slick CSS -->
    <link href="/vendor/slick/slick.css" rel="stylesheet">

    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    
    <!-- Dropzone JS 5.4.0-->
    <script src="/js/dropzone.js"></script>
    <script src="/js/index.js"></script>
    <script src="/vendor/slick/slick.min.js"></script>

<!-- ROOST -->
<link href="/roost/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
<link rel="stylesheet" href="/roost/vendors/bower_components/animate.css/animate.min.css">
<link rel="stylesheet" href="/roost/vendors/bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="/roost/vendors/bower_components/rateYo/src/jquery.rateyo.css">
<!-- <link rel="stylesheet" href="/roost/css/app_1.css">
<link rel="stylesheet" href="/roost/css/app_2.css"> -->

</head>
<body>
    <div id="wrapper">
        @include('partials.header')
        <div id="page-wrapper">
            @include('partials.sideNav')
            <div class="content">
                @yield('content')
            </div>
        </div>
    </div> <!-- wrapper -->

        <!-- <script>
        $(document).ready(function() {

            var myDropzone = new Dropzone("div#uploadBox", {
                url: "/upload",
                autoProcessQuess: false,
                paramName: "files", // The name that will be used to transfer the file
                maxFilesize: 12, // MB
                uploadMultiple: true,
                addRemoveLinks: true,
                // previewsContainer: '.dropzone-previews',

                init: function() {
                    var myDropzone = this;

                            document.querySelector("button[type=submit]").addEventListener("click", function(e) {
                            // Make sure that the form isn't actually being sent.
                            e.preventDefault();
                            e.stopPropagation();
                            // Form check
                            if(checkForm()) {
                                if (fileDropzone.getQueuedFiles().length > 0) {
                                    fileDropzone.processQueue();
                                } else {
                                    setFilesName();
                                    submitForm();
                                }
                            }
                        });

                }


            });
            

        });

        $('#startUpload').click(function(){           
		    myDropzone.processQueue();
	    });

        function previewList(uploadData) {
            $.ajax({
                url:"/upload/list",
                type:'POST',
                data: uploadData,
                success:function(data) {
                    console.log('success');
                    // $('#preview').html(data);
                },
                error:function() {
                    console.log('error');
                }
            });
        }
    </script> -->
</body>
</html>