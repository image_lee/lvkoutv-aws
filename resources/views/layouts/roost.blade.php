<!DOCTYPE html>
<html lang="en">
<!--[if IE 9 ]><html lang="en" class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?=$page_title ?></title>
    
        <!-- Material design colors -->
        <link href="/css/material-design-iconic-font.min.css" rel="stylesheet">

        <!-- CSS animations -->
        <link rel="stylesheet" href="/roost/vendors/bower_components/animate.css/animate.min.css?time=<?=time()?>">

        <!-- Select2 - Custom Selects -->
        <link rel="stylesheet" href="/roost/vendors/bower_components/select2/dist/css/select2.min.css?time=<?=time()?>">

        <!-- Slick Carousel -->
        <link rel="stylesheet" href="/roost/vendors/bower_components/slick-carousel/slick/slick.css?time=<?=time()?>">

        <!-- NoUiSlider - Input Slider -->
        <link rel="stylesheet" href="/roost/vendors/bower_components/nouislider/distribute/nouislider.min.css?time=<?=time()?>">

        <!-- rateYo - Ratings -->
        <link rel="stylesheet" href="/roost/vendors/bower_components/rateYo/min/jquery.rateyo.min.css?time=<?=time()?>">

        <!-- Site -->
       <link rel="stylesheet" href="/roost/css/app_1.css?time=<?=time()?>">
       <link rel="stylesheet" href="/roost/css/app_2.css?time=<?=time()?>">
       <link rel="stylesheet" href="/roost/css/lvkoutv_common.css?time=<?=time()?>">

        <!-- Page Loader JS -->
        <script src="/roost/js/page-loader.min.js?time=<?=time()?>" async></script>

        <script>

            var video = document.getElementById("video"), track;
                hideTracks = function() {
                    for (i = 0; i < video.textTracks.length; i++) {
                        video.textTracks[i].mode = "hidden";
                    }
                };
            hideTracks();
            track = document.createElement("track");
            track.kind = "subtitles";
            track.label = "English";
            track.srclang = "en";
            track.src = "http://img.lvkoutv.com/upload/vtt/pacman-en.vtt";
            video.appendChild(track);
            video.textTracks[2].mode = "showing";
        </script>

        <!-- toastr -->
        <link href="/roost/css/toastr.min.css" rel="stylesheet"/>
        <script src="/roost/js/toastr.min.js"></script>

    </head>
	<body>
        <!-- Start page loader -->
        <!-- <div id="page-loader">
            <div class="page-loader__spinner"></div>
        </div> -->
        <!-- End page loader -->
                
<!--// /////////////////////////////////////////contents start////////////////////////////////////////////////// //-->
		@yield('contents')
<!--// /////////////////////////////////////////contents end  ////////////////////////////////////////////////// //-->

        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
            <div class="ie-warning__inner">
                <ul class="ie-warning__download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="img/browsers/chrome.png" alt="">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="img/browsers/firefox.png" alt="">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="img/browsers/opera.png" alt="">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="img/browsers/safari.png" alt="">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="img/browsers/ie.png" alt="">
                            <div>IE (New)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
        <![endif]-->

        <!-- jQuery -->
        <script src="/js/jquery-3.3.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Waves button ripple effects -->
        <script src="/roost/vendors/bower_components/Waves/dist/waves.min.js?time=<?=time()?>"></script>

        <!-- Select 2 - Custom Selects -->
        <script src="/roost/vendors/bower_components/select2/dist/js/select2.full.min.js?time=<?=time()?>"></script>

        <!-- Slick Carousel - Custom Alerts -->
        <script src="/roost/vendors/bower_components/slick-carousel/slick/slick.min.js?time=<?=time()?>"></script>

        <!-- NoUiSlider - Input Slider -->
        <script src="/roost/vendors/bower_components/nouislider/distribute/nouislider.min.js?time=<?=time()?>"></script>

        <!-- rateYo - Ratings -->
        <script src="/roost/vendors/bower_components/rateYo/min/jquery.rateyo.min.js?time=<?=time()?>"></script>

        <!-- IE9 Placeholder -->
        <!--[if IE 9 ]>
        <script src="/roost/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js?time=<?=time()?>"></script>
        <![endif]-->

        <!-- Site functions and actions -->
        <script src="/roost/js/app.js?time=<?=time()?>"></script>

        <!-- Demo only -->
        <script src="/roost/js/demo/maps/nearby-properties.js?time=<?=time()?>"></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyD_nanUpVqytOmHHfuW4htZsiLH7YUzJ1A" async defer></script> -->
        <script src="/roost/js/demo/demo.js?time=<?=time()?>"></script>

        @if(session()->has('success'))
        <script>
            toastr.success("{{session('success')}}");
        </script>
        @endif

        @if(session()->has('error'))
        <script>
            toastr.warning("{{session('error')}}");
        </script>
        @endif
    </body>
</html>


