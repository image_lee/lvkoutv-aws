<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>吕口TV @yield('title')</title>
    <link rel="icon" type="image/png" href="icon/favicon.png">
    <!-- Bootstrap v4.1.1 -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-grid.min.css" rel="stylesheet">
    <!-- CSS -->
    <link href="/css/index.css" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link href="/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link rel="stylesheet" href="./css/material-design-iconic-font.min.css">
    <link href="./vendors/slick/slick.css" rel="stylesheet">
</head>
<body>
    @yield('authContent')


    
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

    <script src="/js/index.js"></script>
    <script src="./vendors/slick/slick.min.js"></script>
</body>
</html>