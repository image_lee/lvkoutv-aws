@extends('layouts.lvkoutv')
@section('content')

<script>
    $(document).ready(function() {
        console.log('test');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var dropzone = document.getElementById('uploadBox');

        var upload = function(files) {
            console.log(files);
            var formData = new FormData(), xhr = new XMLHttpRequest(),x;

            for (x = 0; x < files.length; x++) {
                formData.append('file[]', files[x]);
            }

            $.ajax({
                type:'POST',
                url:'/upload',
                data: formData,
                processData : false,
                contentType : false,
                success:function(data){
                    alert('success');
                    // alert(data.success);
                },
                error: function() {
                    alert('errer');
                }
            });

            // xhr.onload = function() {
            //     var data = this.responseText();
            //     console.log(data);
            // }
            // xhr.open('post', 'upload', 'true');
            // xhr.send(formData);
        }

        dropzone.ondrop = function(e) {
            e.preventDefault();
            this.className = 'upload-box upload-empty-frame dragover';
            // console.log(e.dataTransfer.files);
            upload(e.dataTransfer.files);
        }

        dropzone.ondragover = function() {
            console.log(this.className);
            this.className = 'upload-box upload-empty-frame dragover';
            return false;
        }

        dropzone.ondragleave = function() {
            this.className = 'upload-box upload-empty-frame';
            return false;
        }
    });

    // $(document).ready(function() {
    //     var form = document.getElementById('uploadFrom');
    //     var xhr = new XMLHttpRequest();

    // });
</script>

<div id="upload" class="upload-section">
    <div class="row">
        <div class="col-sm-12" style="margin-bottom: 24px;">
            <form id="uploadFrom" action="{{ url('upload')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div>
                <div class="upload-box upload-empty-frame upload-table" id="uploadBox">
                    <div class="upload-middle text-center">
                        <div class="btn btn-upload">
                            <span class="choose">Choose videos</span>
                            <input type="file" class="input-file" id="upload-file" name="file[]" multiple>
                        </div>
                        또는 사진을 드래그 하세요
                    </div>
                </div>

            </div>
            </form>
        </div>
        <!-- <div id="preview" class="col-sm-12">
            <div class="card">
                <div class="card-header">Files List</div>
                <div class="dropzone-previews">
                </div>
                <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                    <li id="uploaded"></li>
                    <li class="dropzone-previews text-muted text-center empty">No files uploaded.</li>
                </ul>
            </div>
        </div> -->
    </div>
    <!-- <button id="startUpload">Upload</button> -->
</div>

<!-- <div id="upload" class="upload-section">
    <div class="upload-box">
        <div class="upload-item">
            <img src="/img/icon/middle_upload.jpg"/>
            <form action="{{ url('upload') }}"  method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="file" name="file[]" multiple>
                </div>

                <button type="upload" class="btn btn-upload">Choose video</button>
            </form>
            <p>or drag and drop video files</p>
            <div class="upload-manage">
                <a><h3>video management</h3></a>
            </div>
        </div>
    </div>
</div> -->
<div class="related-upload-section">
    <div class="video-section lk-row">
        <div class="section-title"><h2>Related Upload</h2></div>
        <div class="video-grid-list">
            <div class="listings-grid-item listings-bg-color">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/img/sample/s1.jpg">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item listings-bg-color">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/img/sample/s2.jpg">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item listings-bg-color">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/img/sample/s3.jpg">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item listings-bg-color">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/img/sample/s4.jpg">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
            <div class="listings-grid-item listings-bg-color">
                <a href="#">
                    <div class="listings-grid-main">
                        <img src="/img/sample/s5.jpg">
                        <span class="time">8:00</span>
                    </div>
                    <div class="listings-grid-body">
                        <h3>Alkemilla - Eco COSMETIC</h3>
                        <small>Alkemilla Eco Bio Cosmetic</small>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection



