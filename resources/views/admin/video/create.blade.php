@extends('layouts.admin')
@section('admin_contents')
@include('partials.adminSideNav')

<div id="page-wrapper">
     <div class="row">
         <div class="col-lg-12">
             <h1 class="page-header">동영상 업로드</h1>
         </div>
     </div>
     <div class="row"> <!-- /.row -->
         <div class="col-lg-12">
             <div class="panel panel-default">
                 <div class="panel-heading"></div>
                 <div class="panel-body">
                     <form action="{{ url('admin/video/store') }}" method='post' enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>업로드 파일</label>
                                    <input type="file" name="file" id="file">
                                </div>
                                <div class="form-group">
                                    <label>동영상 제목</label>
                                    <input type="text" id="title" name="title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>동영상 설명</label>
                                    <textarea id="dest" name="dest" rows="3" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>총 시간</label>
                                    <input type="text" id="total_time" name="total_time" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>태그</label>
                                    <input type="text" id="tags" name="tags" class="form-control">                                
                                </div>
                                <div class="form-group">
                                    <label>업로드 할 카테고리</label>
                                    <select id="category" name="category" class="form-control">
                                        <option value='0' selected>Select Category</option>
                                        @foreach($category as $category)
                                            @if( $category->code_group_id == 'VCATEGORY')
                                                <option value="{{ $category->code_name }}">{{ $category->code_name }}</option>
                                            @endif
                                        @endforeach   
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>나라</label>
                                    <select id="country" name="country" class="form-control">
                                        <option value='0' selected>Select country</option>
                                        @foreach($country as $country)
                                            @if( $country->code_group_id == 'VCOUNTRY')
                                                <option value="{{ $country->code_name }}">{{ $country->code_name }}</option>
                                            @endif
                                        @endforeach   
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>공개</label>
                                    <select class="form-control" id="use_yn" name="use_yn">
                                        <option value='1' selected>공개</option>
                                        <option value='0'>비공개</option>
                                    </select>  
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div class="row">
             <div class="col-lg-12">
                <table width="100%">
                 <tr>
                    <td align="left" width="20%"><a class="btn btn-default btn-sm" href="{{ url('/admin/video') }}">목록으로</a></td>
                    <td align="center"><input type="submit" value="저장하기" class="btn btn-primary btn-sm"></td>
                    <td align="right" width="20%"> </td>
                 </tr>
               </table>
               <br/><br/>
             </div>
            </div> <!-- /.row -->
          </form>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection