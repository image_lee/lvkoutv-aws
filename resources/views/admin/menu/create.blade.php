@extends('layouts.admin')
@section('admin_contents')
@include('partials.adminSideNav')
<div id="page-wrapper">
     <div class="row">
         <div class="col-lg-12">
             <h1 class="page-header">시스템 메뉴추가</h1>
         </div>
     </div>
     <div class="row"> <!-- /.row -->
         <div class="col-lg-12">
             <div class="panel panel-default">
                 <div class="panel-heading"></div>
                 <div class="panel-body">
                     <form action="{{ url('admin/menu/store') }}" name="menu_form" method='POST'>
                       <div class="row">
                            <div class="col-lg-6">
                                <!-- <div class="form-group">
                                    <label>메뉴 ID</label>
                                    <input type="hidden" id="channel_id" name="channel_id" value="{{ $channel_id }}" class="form-control">                                
                                </div> -->
                                <div class="form-group">
                                    <label>메뉴 ID</label>
                                    <input type="text" id="subject" name="subject" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>메뉴 명(ko)</label>
                                    <input type="text" id="dest" name="dest" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>메뉴 명(en)</label>
                                    <input type="text" id="ranking" name="ranking" class="form-control">
                                </div>
                                <!-- <div class="form-group">
                                    <label>태그</label>
                                    <input type="text" id="tags" name="tags" class="form-control">                                
                                </div>
                                <div class="form-group">
                                    <label>총 시간</label>
                                    <input type="text" id="total_time" name="total_time" class="form-control">
                                </div> -->
                            </div>
                        </div>
                        <div class="row">    
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <table class="table table-bordered" id="lang_table"> 
                                        <thead>
                                            <tr>
                                                <td><label>자막 언어</label></td>
                                                <td width="30"><button type="button" name="add" class="btn btn-success btn-sm add"><span class="glyphicon glyphicon-plus"></span></button></td>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody">
                                            <tr>
                                                <td>
                                                    <select class="form-control" id="lang_code" name="lang_code_default">
                                                        <option value='0' selected>Select Language</option>                                                          
                                                        @foreach($langs as $lang)
                                                            <option value="{{ $lang->code }}">{{ $lang->origin }}</option>
                                                        @endforeach                               
                                                    </select>  
                                                </td>
                                                <td></td>                                        
                                            </tr>
                                        </tbody>                                   
                                    </table>
                                </div>
                                <!-- <div class="form-group">
                                    <label>자막 언어 2</label>
                                    <select class="form-control" id="lang_code_2" name="lang_code_2">
                                            <option value='0' selected>Select Code</option>                                                          
                                        @foreach($langs as $lang)
                                            <option value="{{ $lang->code }}">{{ $lang->origin }}</option>
                                        @endforeach                               
                                    </select>    
                                </div> -->
                            </div>
                      </div><!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div class="row">
             <div class="col-lg-12">
                <table width="100%">
                 <tr>
                  <td align="left" width="20%"><a class="btn btn-default btn-sm" href="{{ url('/admin/video') }}">목록으로</a></td>
                  <td align="center"><input type="submit" value="저장하기" class="btn btn-primary btn-sm"></td>
                  <td align="right" width="20%"> </td>
                 </tr>
               </table>
               <br/><br/>
             </div>
            </div> <!-- /.row -->
          </form>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection