@extends('layouts.admin')
@section('admin_contents')
@include('partials.adminSideNav')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">시스템 메뉴관리</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    DataTables Advanced Tables
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                        <thead>
                            <tr>
                                <th width="13%">메뉴 ID</th>
                                <th>메뉴 명(ko)</th>
                                <th>메뉴 명(en)</th>
                                <th>메뉴 타입</th>
                                <th  width="10%">로그인</th>
                                <th>정렬</th>
                                <th>공개</th>
                                <th class="center">수정</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($menus as $menu)
                            <tr class="odd gradeX">
                                <td>{{ $menu->menu_id}}</td>
                                <td>{{ $menu->menu_name_ko}}</td>
                                <td>{{ $menu->menu_name_en}}</td>
                                <td class="text-center">{{ $menu->menu_type_code}}</td>
                                <td class="text-center">{{ $menu->login_check}}</td>
                                <td class="text-center">{{ $menu->display_order}}</td>
                                <td class="text-center">{{ $menu->use_yn}}</td>
                                <td class="text-center">
                                    <button id="editBtn" class="btn btn-sm btn-primary"><i class="zmdi zmdi-edit"></i></button>
                                    <!-- <button id="removeBtn" class="btn btn-sm btn-danger"><i class="zmdi zmdi-close"></i></button> -->
                                </td>      
                            </tr>
                        @empty
                            <tr>No menus</tr>
                        @endforelse
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
<!-- /#page-wrapper -->
</div>
@endsection

