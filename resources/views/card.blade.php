<?php
    $page_title = 'Lvkoutv - Beauty video playground'
?>

@extends('layouts.roost')
@section('contents')

<header id="header">
  <!-- Header top -->
  <div class="header__top">
      <div class="container">
      </div>
  </div>
  <!-- End Header top -->

    <div class="header__main">
        <div class="container">
            <a class="logo" href="#!">
                <img src="/roost/img/lvkoutv_logo.png" alt="lvkoutv logo">
                <div class="logo__text">
                    <span>吕口TV</span>
                    <span>Global No.1 Beauty Platform</span>
                </div>
            </a>
            <div class="navigation-trigger visible-xs visible-sm"></div>
        </div>
    </div>
</header>

<script src="/roost/vendors/bower_components/jquery/dist/jquery.min.js?time=<?=time()?>"></script>

 <section class="section">
  <div class="container">
    <header class="section__title">
      <h2>Welcom to LvkouTV videos Playground!</h2>
      <small>Changing the future of the global beauty market</small>
    </header>

    <div class="row">
        <div class="col-sm-12">
            <div style="margin-bottom: 40px;">
                <a href="/roost/video/lvkoutv.mp4" class="media">
                    <div class="listings-grid__main">
                        <img src="/roost/img/lvkoutv1.png" alt="" />
                        <!-- <i class="zmdi zmdi-youtube-play" style="top: 10px;"></i> -->
                    </div>
                </a>  
            </div>
        </div>
        <div class="col-sm-12">
            <div style="margin-bottom: 40px;">
                <a href="/roost/video/jiayou2.mp4" class="media">
                    <div class="listings-grid__main">
                        <img src="/roost/img/lvkoutv2.png" alt="">
                    </div>
                </a>  
            </div>
        </div>
        <div class="col-sm-12">
            <form action="{{ url('card') }}" name="cardForm" method="post" style="padding: 30px; background-color: #f5447a;">
               @csrf
                <header class="section__title">
                    <h4 style="font-size: 18px; color: white;">By sending us your email, <br> you can get China Beauty News</h4>
                </header>
                <div class="form-group form-group--light form-group--float">
                    <input type="text" name="name" class="form-control">
                    <label>Name</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group form-group--light form-group--float">
                    <input type="text" name="email" class="form-control">
                    <label>Email Address</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group form-group--light form-group--float">
                    <input type="text" name="phone" class="form-control">
                    <label>Contact Number</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="m-t-30">
                    <button type="submit" class="btn brn-sm btn-default btn-static">Send</button>
                </div>
            </form>
        </div>
    </div> <!-- row -->
  </div> <!-- container -->
</section>

@endsection

