<div class="lk-header-container lk-h">
    <header class="lk-header">
        <div class="container-fluid">
            <div class="row flex-nowrap">
                <div class="col-md-auto">
                    <div id="lk-logo" class="lk-logo"> 
                        <a href="{{ url('index') }}"><img id="logo-img" width="140" height="37" src="/img/header/lvkoutv_log.png" alt="LvkouTV Logo"></a>
                    </div>
                </div>
                <div class="col-md-auto mr-auto" style="padding-left: 10.3rem;">
                    <div class="lk-search">
                        <form role="search">
                            <div class="form-group search-input">
                                <input id="search" type="text" class="form-control" style=" border: 1px solid #d6d6d6;">
                                <i class="zmdi zmdi-search"></i>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="lk-gnb">
                    <nav class="navbar navbar-expand">
                        <div class="navbar-brand mr-0">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item lk-mega-item">
                                    <a href="{{ url('index') }}" class="nav-link">
                                        <img class="lk-mega-menu" src="/img/header/home_default.png" data-toggle="tooltip" data-placement="bottom" title="Home" onmouseover="this.src='/img/header/home_seclected.png'" onmouseout="this.src='/img/header/home_default.png'" alt="gnb home icon"/>
                                    </a>
                                </li>
                                <li class="nav-item lk-mega-item">
                                    <a href="{{ url('upload') }}" class="nav-link">
                                        <img class="lk-mega-menu" src="/img/header/upload_default.png" data-toggle="tooltip" data-placement="bottom" title="Upload" onmouseover="this.src='/img/header/upload_seclected.png'" onmouseout="this.src='/img/header/upload_default.png'" alt="gnb upload icon"/>
                                    </a>
                                </li>
                                <li class="nav-item lk-mega-item">
                                    <a href="{{ url('beautyNetworks') }}" class="nav-link">
                                        <img class="lk-mega-menu" src="/img/header/network_default.png" data-toggle="tooltip" data-placement="bottom" title="Beauty networks" onmouseover="this.src='/img/header/network_selected.png'" onmouseout="this.src='/img/header/network_default.png'" alt="gnb network icon"/>
                                    </a>
                                </li>
                                <li class="nav-item lk-mega-item">
                                    <a href="{{ url('jobs') }}" class="nav-link">
                                        <img class="lk-mega-menu" src="/img/header/jobs_default.png" data-toggle="tooltip" data-placement="bottom" title="Jobs" onmouseover="this.src='/img/header/jobs_selected.png'" onmouseout="this.src='/img/header/jobs_default.png'" alt="gnb jobs icon"/>
                                    </a>
                                </li>
                                <li class="nav-item lk-mega-item">
                                    <a href="{{ url('news') }}" class="nav-link">
                                        <img class="lk-mega-menu" src="/img/header/news_default.png" data-toggle="tooltip" data-placement="bottom" title="China News" onmouseover="this.src='/img/header/news_selected.png'" onmouseout="this.src='/img/header/news_default.png'" alt="gnb news icon"/>
                                    </a>
                                </li>  
                                <li class="nav-item lk-mega-item">
                                    <a href="{{ url('notifications') }}" class="nav-link">
                                        <img class="lk-mega-menu" src="/img/header/notice_default.png" data-toggle="tooltip" data-placement="bottom" title="Notifications" onmouseover="this.src='/img/header/notice_selected.png'" onmouseout="this.src='/img/header/notice_default.png'" alt="gnb notice icon"/>
                                    </a>
                                </li>
                                <li class="nav-item lk-mega-item lk-login">
                                    <a href="{{ url('user/signIn') }}" class="nav-link">SIGN IN</a>
                                </li>
                            
                                <!-- <li class="nav-item lk-mega-item lk-profile">
                                    <a href="javascript:void(0)" class="nav-link dropdown-toggle" role="button" id="dropdownMe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="lk-mega-menu" src="/img/blank_profile.png" alt="gnb profile icon"/>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-animation lk-dropdown-me" aria-labelledby="dropdownMe">
                                        <a class="dropdown-item lk-dropdown-me-header" href="javascript:void(0)">
                                            <img width="40" height="40" src="/img/blank_profile.png"/>
                                            <div class="lk-me-body media-body">
                                                <h3>leemj9213@gmail.com</h3>
                                                <p>Lvkoutv</p>
                                            </div>
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="./channel-biz-home.html">My Channel</a>
                                        <a class="dropdown-item" href="./channel-biz-home.html">+ Company Channel</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Setting</a>
                                        <a class="dropdown-item" href="#">Language</a>
                                        <a class="dropdown-item" href="#">Help</a>
                                        <a class="dropdown-item" href="#">Logout</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">My premiun</a>
                                        <a class="dropdown-item" href="#">My posts & activity</a>
                                    </div>
                                </li>                                                 -->
                            </ul>
                        </div>
                    </nav>
                </div>
            </div> <!-- row -->
        </div>
    </header>
</div> <!-- lk-header-container -->
