
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="{{url('/admin/user')}}">LvkouTV Admin</a>
		</div>
	<!-- /Navigation -->
		<ul class="nav navbar-top-links navbar-right">
			<!-- dropdown -->
			<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
			    <li><a href="{{url('/admin/user')}}"><i class="fa fa-user fa-fw"></i> Admin Login</a></li>
			    <li class="divider"></li>
			    <li><a href="{{url('/admin/logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
			<!-- /.dropdown-user -->
			</li>
			<!-- /.dropdown -->
		</ul>

	<!-- side bar -->
	<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
	<ul class="nav" id="side-menu">
	    <li class="sidebar-search">
			<form action="{{url('/admin/search')}}" method='post'>
	        <div class="input-group custom-search-form">
	           <input type="text" class="form-control" name="search" placeholder="Search..."/>
	            <span class="input-group-btn" >
					<i class='fa fa-search'>
						<input type="submit" class="btn btn-default" value="검색" />
					</i>
	            </span>
	        </div>
				</form>
	        <!-- /input-group -->
	    </li>

	<li><a href="{{url('/admin/menu')}}"><i class="fa fa-table fa-fw"></i>시스템 메뉴관리<span class="fa arrow"></span></a></li>
    <li><a href="{{url('/admin/channel')}}"><i class="fa fa-table fa-fw"></i>채널 관리<span class="fa arrow"></span></a></li>
	<li><a href="{{url('/admin/video')}}"><i class="fa fa-table fa-fw"></i> 비디오 관리<span class="fa arrow"></span></a></li>
	<li><a href="{{url('/admin/questionnaire')}}"><i class="fa fa-table fa-fw"></i> 설문지 리스트<span class="fa arrow"></span></a></li>
	<li><a href="{{url('/admin/category')}}"><i class="fa fa-table fa-fw"></i> 카테고리 관리<span class="fa arrow"></span></a></li>	

	</ul>		
  </nav>
<!-- /navbar header -->
