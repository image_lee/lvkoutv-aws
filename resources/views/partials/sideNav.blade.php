<div class="lk-side-nav side-scroll">
    <ul class="lk-sidemenu">
        <li class="lk-side-main-item"><a href="/" class="lk-side-link">Home</a></li>
        <li class="lk-side-main-item"><a href="{{ url('/trending') }}" class="lk-side-link">Trending</a></li>
        <!-- <li class="lk-side-main-item"><a href="{{ url('/live') }}" class="lk-side-link">Live</a></li>
        <li class="lk-side-main-item"><a href="{{ url('/feed') }}" class="lk-side-link">News Feed</a></li> -->
        <li class="lk-side-main-item"><a href="#" class="lk-side-link">Live</a></li>
        <li class="lk-side-main-item"><a href="#" class="lk-side-link">News Feed</a></li>
    </ul>
    <ul class="lk-sidemenu">
        <li class="lk-side-nav-title">Business</li>
        <li class="lk-side-item"><a href="{{ url('business/distributor') }}" class="lk-side-link">Distributor</a></li>
        <li class="lk-side-item"><a href="{{ url('business/weishang') }}" class="lk-side-link">Weishang</a></li>
        <li class="lk-side-item"><a href="{{ url('business/brand') }}" class="lk-side-link">Brand</a></li>
        <li class="lk-side-item"><a href="{{ url('business/mkt') }}" class="lk-side-link">Beauty MKT/PR</a></li>
        <li class="lk-side-item"><a href="{{ url('business/odm') }}" class="lk-side-link">ODM/OEM/OBM</a></li>
        <li class="lk-side-item"><a href="{{ url('business/logistics') }}" class="lk-side-link">Logistics</a></li>
    </ul>
    <ul class="lk-sidemenu">
        <li class="lk-side-nav-title">People</li>
        <li class="lk-side-item"><a href="{{ url('people/consultant') }}" class="lk-side-link">Beauty consultant</a></li>
        <li class="lk-side-item"><a href="{{ url('people/influencer') }}" class="lk-side-link">Beauty influencer / KOL</a></li>
    </ul>
    <ul class="lk-sidemenu">
        <li class="lk-side-nav-title">How To</li>
        <li class="lk-side-item"><a href="{{ url('howto/skincare') }}" class="lk-side-link">Skin Care</a></li>
        <li class="lk-side-item"><a href="{{ url('howto/makeup') }}" class="lk-side-link">Make Up</a></li>
        <li class="lk-side-item"><a href="{{ url('howto/review') }}" class="lk-side-link">Review</a></li>
    </ul>
    <ul class="lk-sidemenu">
        <li class="lk-side-nav-title">My</li>
        <li class="lk-side-item"><a href="#" class="lk-side-link">Watch Later</a></li>
        <li class="lk-side-item"><a href="mylike" class="lk-side-link">Like Video</a></li>
    </ul>
</div>


