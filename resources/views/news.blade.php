@extends('layouts.lvkoutv')
@section('content')

<section class="news-section lk-row">
    <!-- <div class="container"> -->
        <header class="section__title">
            <!-- <h5>Today's China Beauty News!</h5> -->
            <!-- <small>Vestibulum id ligula porta felis euismod semper</small> -->
        </header>

        <div class="row">
            <div class="col-md-7 col-sm-7">
                <article class="card">
                    <a class="card__img" href="blog-details.html">
                        <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/emma-1544533784.jpg?crop=1.00xw:1.00xh;0,0&resize=980:*" alt="">
                    </a>
                    <div class="card__header">
                        <h2>7 Strawberry-Blonde Hair Color Ideas Inspired by Your Favorite Celebs</h2>
                        <small>by Sam Andersion on 17th July 2016</small>
                    </div>
                    <div class="card__body">
                    It's no secret that with every new year comes a fresh set of hair-color trends (and a fresh set of failed resolutions, but hey). And as we approach 2019, you can expect to see a spike in rich, strawberry-blonde hues—i.e. the only bright light in this dark, gloomy season. So before the new year's prettiest shade grows dim, get inspired to try it at home with one of these seven celebrity takes, ahead.
                        <div class="blog-more">
                            <a href="blog-details.html">Read More...</a>
                        </div>
                    </div>
                </article>

                <article class="card">
                    <a class="card__img" href="blog-details.html">
                        <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/bronzer-1544724428.gif?crop=1xw:1xh;center,top&resize=768:*" alt="">
                    </a>
                    <div class="card__header">
                        <h2>
The 7 Best Drugstore Bronzers That'll Make You Glow All Winter Long</h2>
                        <small>by Dave Rubin on 15th July 2016</small>
                    </div>
                    <div class="card__body">
                    I don't know about you, but mid-December is typically when I start digging through my makeup bin to bust out my bronzer—@ me if you feel me—especially since my summer glow has definitely diminished by then, leaving me longing for sun-kissed skin. Cue these seven drugstore gems that will impart the most perfect glow without making you look streaky or orange.
                        <div class="blog-more">
                            <a href="blog-details.html">Read More...</a>
                        </div>
                    </div>
                </article>

                <article class="card">
                    <a class="card__img" href="blog-details.html">
                        <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/best-bronzers-1544807944.png?crop=1.00xw:1.00xh;0,0&resize=768:*" alt="">
                    </a>
                    <div class="card__header">
                        <h2>The 5 Best Bronzers That Actually Look Natural</h2>
                        <small>by Malinda Hollaway on 10th July 2016</small>
                    </div>
                    <div class="card__body">
                    In my dreams, the air is warm, my skin is bronzed, and I've spent the last few days in the Antibes, practicing my broken French with the locals. But this is reality, and IRL, the air is a blizzard, my face is washed out, and the only thing I've practiced is how to avoid my aunt's questions about my love life during the holidays. 
                    <div class="blog-more">
                            <a href="blog-details.html">Read More...</a>
                        </div>
                    </div>
                </article>
            </div>

            <aside class="col-md-4 col-sm-5 hidden-xs">
                <!-- <div class="card subscribe" style="background-color: #FFB74D;">
                    <div class="subscribe__icon">
                        <i class="zmdi zmdi-email"></i>
                    </div>
                    <h2>Subscribe for Newsletters</h2>
                    <small>By sending us your email, you can get China Beauty News</small>

                    <form>
                        <div class="form-group form-group--light form-group--float">
                            <input type="text" class="form-control text-center" placeholder="Email Address">
                            <i class="form-group__bar"></i>
                        </div>

                        <button class="btn btn--circle">
                            <i class="zmdi zmdi-check mdc-text-orange-400"></i>
                        </button>
                    </form>
                </div> -->

                <!-- <div class="card">
                    <div class="card__header">
                        <h2>Picked articles</h2>
                        <small>Morbi risus porta consectetur vestibulum</small>
                    </div>

                    <div class="list-group">
                        <a href="" class="list-group-item media">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="" class="list-group__img" width="65">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Vivamus sagittis lacus</strong>
                                <small>Cras mattis consectetur purus sit amet fermentum</small>
                            </div>
                        </a>

                        <a href="" class="list-group-item media">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="" class="list-group__img" width="65">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Fusce dapibus tellusac</strong>
                                <small>Praesent commodo cursus magna, vel scelerisque nisl consectetur et</small>
                            </div>
                        </a>

                        <a href="" class="list-group-item media">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="" class="list-group__img" width="65">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Praesent commodo cursus magnavel scelerisque nisl</strong>
                                <small>Maecenas sed diam eget risus varius blandit sit amet non magna</small>
                            </div>
                        </a>

                        <a href="" class="list-group-item media">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="" class="list-group__img" width="65">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Lorem ipsum dolor sitamet</strong>
                                <small>Donec ullamcorper nulla non metus auctor fringilla</small>
                            </div>
                        </a>

                        <a href="" class="list-group-item media">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="" class="list-group__img" width="65">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Fusce dapibus accursus commodo</strong>
                                <small>Aenean lacinia bibendum nulla sed consectetur</small>
                            </div>
                        </a>

                        <div class="p-10"></div>
                    </div>
                </div> -->

                <!-- <div class="card tags-list">
                    <div class="card__header">
                        <h2>Categories</h2>
                        <small>Nulla vitae elit libero, a pharetra augue</small>
                    </div>
                    <div class="card__body">
                        <a href="" class="tags-list__item">#Sollicitudin</a>
                        <a href="" class="tags-list__item">#Ultricies</a>
                        <a href="" class="tags-list__item">#Elit</a>
                        <a href="" class="tags-list__item">#Crastortor</a>
                        <a href="" class="tags-list__item">#Condimentum</a>
                        <a href="" class="tags-list__item">#Purus</a>
                        <a href="" class="tags-list__item">#Purus</a>
                        <a href="" class="tags-list__item">#Egestas</a>
                        <a href="" class="tags-list__item">#Loremipsum</a>
                        <a href="" class="tags-list__item">#Ridiculus</a>
                        <a href="" class="tags-list__item">#Ipsum</a>
                        <a href="" class="tags-list__item">#Consectetur</a>
                    </div>
                </div> -->

                <!-- <div class="card">
                    <div class="card__header">
                        <h2>Recent comments</h2>
                        <small>Etiam porta sem malesuada magna mollis</small>
                    </div>
                    <div class="list-group">
                        <a class="list-group-item media" href="">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="" class="list-group__img img-circle" width="45" height="45">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Sarah Zelermyer Diaz</strong>
                                <small class="list-group__text">Nullam quis risus eget urna mollis ornare vel eu leo</small>
                            </div>
                        </a>

                        <a class="list-group-item media" href="">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="" class="list-group__img img-circle" width="45" height="45">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Malinda Hollaway</strong>
                                <small class="list-group__text">Vestibulum id ligula porta felis euismod semper</small>
                            </div>
                        </a>

                        <a class="list-group-item media" href="">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="" class="list-group__img img-circle" width="45" height="45">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Celine Diaz</strong>
                                <small class="list-group__text">Cras justo odio, dapibus ac facilisis</small>
                            </div>
                        </a>

                        <a class="list-group-item media" href="">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="" class="list-group__img img-circle" width="45" height="45">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Lion Sampson</strong>
                                <small class="list-group__text">Integer posuere erat a ante venenatis dapibus posuere velit aliquet</small>
                            </div>
                        </a>

                        <a class="list-group-item media" href="">
                            <div class="pull-left">
                                <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="" class="list-group__img img-circle" width="45" height="45">
                            </div>
                            <div class="media-body list-group__text">
                                <strong>Thomase Read</strong>
                                <small class="list-group__text">Donec sed odio dui</small>
                            </div>
                        </a>

                        <div class="p-10"></div>
                    </div>
                </div> -->
            </aside>
        </div>
    <!-- </div> -->
</section>

@endsection