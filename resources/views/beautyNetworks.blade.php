@extends('layouts.lvkoutv')
<!-- @yield('title', 'Welcome to 吕口TV') -->
@section('content')
<div class="row lk-row">
    <div class="float-left">
        <div class="profile-section lk-card profile">
            <div class="profile-img">
                <img src="/img/people/12.jpg" alt="">
            </div>
            <div class="profile__info">
                <span class="label label-warning">Company</span>
                <span class="label label-success">Premium Member</span>

                <div class="profile__review">
                    <span>Jermaine S. Wilson / Distributor</span>
                </div>
                <ul class="rmd-contact-list">
                    <li><i class="zmdi zmdi-face"></i>Followers: 124</li>
                    <li><i class="zmdi zmdi-phone"></i>308-360-8938</li>
                    <li><i class="zmdi zmdi-email"></i>malinda@inbound.plus</li>
                </ul>
            </div>
        </div>
        <div class="add-friend-section">
                        <div class="add-desc">
                            <h5>Add Friends</h5>
                            <p>We’ll import and store your contacts to suggest connections and show you relevant updates.</p>
                        </div>
                        <form class="add-form">
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label col-form-label-sm">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control form-control-sm" id="email">
                                </div>                                       
                            </div>
                            <div class="form-group row">
                                <label for="pwd" class="col-sm-3 col-form-label col-form-label-sm">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control form-control-sm" id="pwd">
                                </div>
                            </div>
                            <div class="float-right">
                                <button type="submit" class="btn btn-add-submit">Find</button>
                            </div>
                        </form>

                    </div>   
    </div>
    <div class="float-left">
        <div class="beauty-btn-section">
            <div class="btn-row d-flex flex-row">
                    <button class="btn btn-beauty">Seller <span class="btn-count">(2/100)</span></button>
                    <button class="btn btn-beauty">Buyer <span class="btn-count">(2/100)</span></button>
                    <button class="btn btn-beauty">ODM / OEM <span class="btn-count">(2/100)</span></button>
                    <button class="btn btn-beauty">MRT / PR <span class="btn-count">(2/100)</span></button>
            </div>
            <div class="btn-row d-flex flex-row">
                <button class="btn btn-beauty">KOL / Influencer <span class="btn-count">(2/100)</span></button>
                <button class="btn btn-beauty">Logistics <span class="btn-count">(2/100)</span></button>
                <button class="btn btn-beauty">Others <span class="btn-count">(2/100)</span></button>
                <!-- <button class="btn btn-beauty">Seller</button> -->
            </div>
        </div>   
        
        <div class="network-section">
            <div style="margin-left: 10px;"><h5>Recommand</h5></div>
            <div class="trending-channel-list justify-content-center mb-0">
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">
                        <div class="overlay-container">
                            <img src="/img/people/2.jpg" class="channel-img"alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Seller</small>
                            </div>
                            <div class="overlay-to-top">
                                <!-- <p style="margin: 0px;">
                                    <a class="zmdi zmdi-favorite-outline"></a>
                                </p> -->
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img src="/img/people/1.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Distributor</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img src="/img/people/3.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Developer</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="trending-channel-list justify-content-center mb-0">
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img src="/img/people/4.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Buyer</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img  src="/img/people/5.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Chairman</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img  src="/img/people/6.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>KOL</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>    
                </div>
            </div>
            <div class="trending-channel-list justify-content-center mb-0">
                <div class="trending-channel-item chsize bgcolor" style="margin: 0px 5px;">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img  src="/img/people/7.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Designer</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>    
                </div>
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img  src="/img/people/8.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Chairman / Chief Executive Officer</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="trending-channel-item chsize bgcolor mr-0">
                    <a href="javascript:void(0)">                                
                        <div class="overlay-container">
                            <img  src="/img/people/9.jpg" class="channel-img" alt="">
                            <div class="channel-info">
                                <h4>Jermaine S. Wilson</h4>
                                <small>Chairman / Chief Executive Officer</small>
                            </div>
                            <div class="overlay-to-top">
                                <div class="actions listings-grid__favorite">
                                    <div class="actions__toggle">
                                        <input type="checkbox">
                                        <i class="zmdi zmdi-favorite-outline"></i>
                                        <i class="zmdi zmdi-favorite"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>    
                </div>
            </div>
        </div>
    </div>
</div>








@endsection