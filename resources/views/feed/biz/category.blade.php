@extends('layouts.lvkoutv')
@section('content')
<div class="lk-row" style="background: #ececec">
    <div class="category-header">
        <li class="list-inline-item"><span class="category-header-title">Category Distribuor</span></li>
    </div>
    <div class="row lk-row justify-content-center">
        <div class="category-list-item">
            <div class="overlay-container">
                <div class="best"><img src="/img/best2.png"></div>
                <img width="330" height="230" src="/img/thumb/brand/100.png" alt="ads image">
                <div class="overlay-bottom category-ol-bottom">
                    <div class="text">
                        <h3 class="title">MATTE Brand PR Video</h3>
                        <!-- <small>Cosmax china</small> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="category-list-item">
            <div class="overlay-container">
                <div class="best"><img src="/img/best2.png"></div>
                <img width="330" height="230" src="/img/thumb/brand/101.png" alt="ads image">
                <div class="overlay-bottom category-ol-bottom">
                    <div class="text">
                        <h3 class="title">Master Class Brand PR</h3>
                        <!-- <small>Cosmax china</small> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="category-list-item">
            <div class="overlay-container">
                <div class="best"><img src="/img/best2.png"></div>
                <img width="330" height="230" src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_12 TILLEY_EN_호주.png" alt="ads image">
                <div class="overlay-bottom category-ol-bottom">
                    <div class="text">
                        <h3 class="title">Tilley_2018. May</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="lk-row">
    <div class="cat-video-section">
        <div class="list-group">
            <a href="{{ url('video/detail/6') }}" class="lk-list-group-itelm media">
                <div class="lk-list-group-img float-left">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_27 Little Butterfly_영국.png" alt="" width="250">
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Little Butterfly_2018. May</strong>
                    <small>UK</small>
                    <small>French Terrake is a pure original imported brand Terrake法国天芮他是一个纯原装进口品牌</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>3</span>
                    </div>    
                </div>
            </a>
            <a href="{{ url('video/detail/7') }}" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_13 VIRIDIS_Mirko Buffini_EN_이탈리아.png" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Mirko Buffini_2018. May</strong>
                    <small>Italy</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                                                        <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                    <span>377, 464</span>
                    </div>    
                </div>
            </a>
            <a href="{{ url('video/detail/8') }}" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="/img/thumb/brand/180718 루커TV 박람회 리포팅_6_5 Perfect Coll.png" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Perfect Coll_2018. May</strong>
                    <small>Poland</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                        <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>    
                </div>
            </a>
            <a href="{{ url('video/detail/9') }}" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_40 S.Ishira_카타르.png" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>Poland</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                        <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>    
                </div>
            </a>
            <a href="{{ url('video/detail/11') }}" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_19 Bxpression_EN_미국.png" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Bxpression_2018. March</strong>
                    <small>USA</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                        <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>    
                </div>
            </a>
            <a href="{{ url('video/detail/12') }}" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="/img/thumb/brand/180723 루커TV 박람회 리포팅_7_26 JBP_EN_일본.png" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>Japan</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                        <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>    
                </div>
            </a>
        </div>
    </div>
</div>

<!-- <div class="lk-row">
    <div class="cat-video-section">
        <div class="list-group">
            @forelse($brands as $brand)
            <a href="" class="lk-list-group-itelm media" style="padding-bottom: 20px;">
                <div class="lk-list-group-img float-left">
                    <img src="{{ $brand->video_thumbnail_src }}" alt="" width="250">
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>{{ $brand->video_title }}</strong>
                    <small>{{ $brand->video_country }}</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>    
                </div>
            </a>
            @empty
                <li>error</li>
            @endforelse 
        </div>
    </div>
</div> -->

@endsection
