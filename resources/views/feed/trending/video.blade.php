@extends('layouts.lvkoutv')
@section('content')

<div class="trending-header">
    <!-- <li class="list-inline-item"><span class="trending-header-title">Trending</span></li> -->
    <ul class="list-unstyled">
        <li class="list-inline-item"><button type="button" class="btn btn-channel">Channel</button></li>
        <li class="list-inline-item"><button type="button" class="btn btn-video">Video</button></li>
    </ul>
</div>
<div class="lk-row"> 
    <div class="ch-section-title"><h2>Top 10 Company channel</h2></div>
    <div class="trending-channel-list">
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">1</span>
                <img src="/img/logo/14.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                <div class="overlay-to-top">
                    <!-- <p style="margin: 0px;">
                        <a class="zmdi zmdi-favorite-outline"></a>
                    </p> -->
                    <div class="actions listings-grid__favorite">
                        <div class="actions__toggle">
                            <input type="checkbox">
                            <i class="zmdi zmdi-favorite-outline"></i>
                            <i class="zmdi zmdi-favorite"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">2</span>
                <img src="/img/logo/10.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">3</span>
                <img src="/img/logo/11.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">4</span>
                <img src="/img/logo/12.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">5</span>
                <img src="/img/logo/13.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ch-section-title"><h2>Top 10 Person channel</h2></div>
    <div class="trending-channel-list">
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">1</span>
                <img src="/img/people/11.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">2</span>
                <img src="/img/people/10.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">3</span>
                <img src="/img/people/9.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">4</span>
                <img src="/img/people/8.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">5</span>
                <img src="/img/people/7.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ch-section-title"><h2>Top 10 Distributor channel</h2></div>
    <div class="trending-channel-list">
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">1</span>
                <img src="/img/logo/18.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">2</span>
                <img src="/img/logo/19.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">3</span>
                <img src="/img/logo/20.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">4</span>
                <img src="/img/logo/21.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="trending-channel-item chsize">
            <div class="overlay-container">
                <span class="rank">5</span>
                <img src="/img/logo/22.jpg" class="channel-img" alt="">
                <div class="channel-info">
                    <h4>Jermaine S. Wilson</h4>
                    <small>Chairman / Chief Executive Officer</small>
                </div>
                <a class="overlay-link" href="#"></a>
                    <div class="overlay-to-top">
                        <div class="actions listings-grid__favorite">
                            <div class="actions__toggle">
                                <input type="checkbox">
                                <i class="zmdi zmdi-favorite-outline"></i>
                                <i class="zmdi zmdi-favorite"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection