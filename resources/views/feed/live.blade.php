@extends('layouts.lvkoutv')
@section('content')
<div class="row lk-row">
    <div class="float-left">
        <div class="profile-section lk-card profile">
            <div class="profile-img">
                <img src="./img/sample/ch2.png" alt="">
            </div>
            <div class="profile-info">
                <ul class="profile-info-list">
                    <li>Jermaine S. Wilson</li>
                    <li>Distributor</li>
                    <li>My Followers: 8</li>
                </ul>
            </div>
            <button class="btn btn-profile">Your Interest</button>
        </div>  
    </div>

    <div class="float-left">
        <div class="feed-section">

        </div>

    </div>
</div>
@endsection