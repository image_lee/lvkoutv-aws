@extends('layouts.lvkoutv')
@yield('title', 'Welcome to 吕口TV')
@section('content')
<div class="trending-header">
    <li class="list-inline-item"><span class="trending-header-title">My</span></li>
    <ul class="list-unstyled">
        <li class="list-inline-item"><button type="button" class="btn btn-channel">Liked</button></li>
        <li class="list-inline-item"><button type="button" class="btn btn-video">Watch Later</button></li>
    </ul>
</div>
<div class="lk-row">
    <div class="mylike-section">
        <div class="list-group">
            <a href="#" class="lk-list-group-item">
                <div class="lk-list-group-img float-left">
                    <img src="./img/sample/s12.jpg" alt="" width="250">
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>channel name</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>
                    <!-- <button class="mylike-favorite"></button> -->
                    <div class="actions listings-grid__favorite">
                        <div class="actions__toggle">
                            <input type="checkbox">
                            <i class="zmdi zmdi-favorite-outline"></i>
                            <i class="zmdi zmdi-favorite"></i>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="./img/sample/s14.jpg" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>channel name</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>
                    <div class="actions listings-grid__favorite">
                        <div class="actions__toggle">
                            <input type="checkbox">
                            <i class="zmdi zmdi-favorite-outline"></i>
                            <i class="zmdi zmdi-favorite"></i>
                        </div>
                    </div>   
                </div>
            </a>
            <a href="#" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="./img/sample/s15.jpg" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>channel name</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>
                    <div class="actions listings-grid__favorite">
                        <div class="actions__toggle">
                            <input type="checkbox">
                            <i class="zmdi zmdi-favorite-outline"></i>
                            <i class="zmdi zmdi-favorite"></i>
                        </div>
                    </div>   
                </div>
            </a>
            <a href="#" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="./img/sample/s12.jpg" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>
                    <div class="actions listings-grid__favorite">
                        <div class="actions__toggle">
                            <input type="checkbox">
                            <i class="zmdi zmdi-favorite-outline"></i>
                            <i class="zmdi zmdi-favorite"></i>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="./img/sample/s14.jpg" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>channel name</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>    
                    <div class="actions listings-grid__favorite">
                        <div class="actions__toggle">
                            <input type="checkbox">
                            <i class="zmdi zmdi-favorite-outline"></i>
                            <i class="zmdi zmdi-favorite"></i>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#" class="lk-list-group-item media">
                <div class="lk-list-group-img float-left">
                    <img src="./img/sample/s15.jpg" alt="" width="250">
                    <div class="time">10:30</div>
                </div>
                <div class="media-body lk-list-group-text">
                    <strong>Alkemilla Eco Bio Cosmetic</strong>
                    <small>channel name</small>
                    <small>Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec id elit non mi porta gravida at eget metus.</small>
                    <div class="view">
                        <i class="zmdi zmdi-accounts-outline"></i>
                        <span>377, 464</span>
                    </div>    
                    <div class="actions listings-grid__favorite">
                        <div class="actions__toggle">
                            <input type="checkbox">
                            <i class="zmdi zmdi-favorite-outline"></i>
                            <i class="zmdi zmdi-favorite"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
@endsection