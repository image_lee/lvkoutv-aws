<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

$gctl = 'MainController';
$fctl = 'FeedController';

Route::get('/', 'FeedController@index');
Route::get('index', 'FeedController@index');
Route::get('trending', 'FeedController@trending');
Route::get('live', 'FeedController@live');
Route::get('feed', 'FeedController@feed');
Route::get('business/distributor', 'FeedController@category');
Route::get('business/weishang', 'FeedController@category');
Route::get('business/brand', 'FeedController@category');
Route::get('business/mkt', 'FeedController@category');
Route::get('business/odm', 'FeedController@category');
Route::get('people/consultant', 'FeedController@category');
Route::get('people/influencer', 'FeedController@category');
Route::get('howto/skincare', 'FeedController@category');
Route::get('howto/makeup', 'FeedController@category');
Route::get('howto/review', 'FeedController@category');

// Route::get('business/brand', 'FeedController@category');

Route::get('card', 'RandingController@index');
Route::post('card', 'RandingController@card');

Route::get('upload', $gctl.'@upload');
Route::get('beautyNetworks', $gctl.'@network');
Route::get('jobs', $gctl.'@jobs');
Route::get('news', $gctl.'@news');
Route::get('notifications', $gctl.'@notifications');
Route::get('video/detail/{id?}',  $gctl.'@video');

Route::get('side', 'SystemController@sideNav');

// Route::get('user/verify/{verification_code}', 'AuthController@verifyUser');
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
// Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');

Route::post('menu/create', 'SystemController@test')->name('menu');
Route::post('program/create', 'SystemController@test')->name('program');
Route::post('code_group/create', 'SystemController@test')->name('code_group');

Route::prefix('admin')->group(function () {

    $ctl = 'Admin\SystemController';
    Route::get ('', $ctl.'@index'); // menu list form 
    Route::get ('menu/create', $ctl.'@create'); 
    Route::post('menu/store', $ctl.'@store');
    Route::get ('menu/{id}', $ctl.'@show'); //show id 상세보기
    Route::get ('menu/edit/{id}', $ctl.'@show'); //show id 상세보기
    Route::put ('menu/{id}', $ctl.'@update'); //show id 상세보기
});

Route::prefix('admin/video')->group(function () {

    $ctl = 'FileController';
    Route::get ('', $ctl.'@index'); // menu list form 
    Route::get ('create', $ctl.'@create'); 
    Route::post('store', $ctl.'@store');
    Route::get ('{id}', $ctl.'@show'); //show id 상세보기
    Route::get ('edit/{id}', $ctl.'@show'); //show id 상세보기
    Route::put ('{id}', $ctl.'@update'); //show id 상세보기
});



Route::get('user/signIn', 'AuthController@signInForm');
Route::view('user/signUp', 'user.signUp');

// Route::get('upload', 'UploadController@index');
Route::get('test', 'UploadController@test');
Route::post('test', 'UploadController@store');

// Route::post('upload', 'UploadController@store'); // 파일에 대한 정보(주제, 설명)을 디비에 저장하는 기능   
//Route::post('upload', 'UploadController@uploadList'); // 업로드한 파일을 s3에다가 올리는 기능


Route::get('password/find', 'AuthController@findEmail'); //get, 이메일 계정 찾는 부분 
Route::post('password/resetSentEmail', 'AuthController@resetSentEmail'); //이메일 입력 후 보내는 폼 
Route::post('password/reset', 'AuthController@resetPassword'); //get, 이메일 계정 찾는 부분 
Route::view('password/resetSuccess', 'user.password.resetSuccess'); //get, 이메일 계정 찾는 부분 

Route::post('channel/category/{$id}', 'UploadController@upload'); // 업로드한 파일을 s3에다가 올리는 기능
Route::view('interest', 'user.interest'); 







