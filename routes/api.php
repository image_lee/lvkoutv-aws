<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('user/signIn', 'AuthController@login');
Route::post('user/signUp', 'AuthController@signUp');

// Route::post('forgetPassword', 'AuthController@recover');


Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('user', 'AuthController@getAuthUserData');
    Route::post('logout', 'AuthController@logout');
});





